import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "products",
      component: () =>
        import("./views/Products.vue")
    },
    {
      path: "/categories",
      name: "categories",
      component: () =>
        import("./views/Categories.vue")
    },
    {
      path: "/prices",
      name: "prices",
      component: () =>
        import("./views/Prices.vue")
    },
    {
      path: "/quantities",
      name: "quantities",
      component: () =>
        import("./views/Quantities.vue")
    },
    {
      path: "/P_methods",
      name: "p_methods",
      component: () =>
        import("./views/PaymentMethods.vue")
    },
    {
      path: "/sales",
      name: "sales",
      component: () =>
        import("./views/Sales.vue")
    },
  ]
});
