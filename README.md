<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Mobi Mall Vue</h3>

  <p align="center">
    A VueJs frontend for a simple sales application
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Contact](#contact)


## About The Project

This is the web frontend of a simple sales application built with django rest framework.
It consumes the API built by the latter.



### Built With

* []()VueJs
* []()Bootstrap Vue
* []()Axio



## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* Node.js 8.11.0+

### Installation
 
1. Clone the repo
```sh
git clone https://timobure@bitbucket.org/timobure/mobimalldjango.git
```
2. Install packages
```sh
npm install -g @vue/cli
```
3. Install dependencies

Fire up the npm server

## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

Timon Obure - timobure@gmail.com

Project Link: https://timobure@bitbucket.org/timobure/mobimallvue.git